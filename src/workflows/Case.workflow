<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_case_owner</fullName>
        <description>Send email to case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Product_Inquiry</template>
    </alerts>
    <rules>
        <fullName>Intimate Case Owner</fullName>
        <actions>
            <name>Send_email_to_case_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Intimate case owner when the product is ready for replacement</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
