/*##############################################################################
########################## Light Trigger Handler #############################################

##Developed By : R200
##Company : ET Marlabs

##################################################################################
################################################################################*/
public class Light_Trigger_Handler 
{
    public static void lightsCOntroller(list<FeedItem> lstFeedItem){
        for(FeedItem f : lstFeedItem){
        if (f.Body.contains('ON'))
        {
            List<Light__c> officeLights = [select Id, State__c from Light__c where PIN__c = 13 limit 1];
            for(Light__c l:officeLights){
                l.State__c = 'ON';
            }

            update officeLights;
        }


        if (f.Body.contains('OFF'))
        {
            List<Light__c> officeLights = [select Id, State__c from Light__c where PIN__c = 13 limit 1];
            for(Light__c l:officeLights){
                l.State__c = 'OFF';
            }

            update officeLights;
        }
        }
    }   
}