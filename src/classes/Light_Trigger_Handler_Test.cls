@isTest
public class Light_Trigger_Handler_Test {
	
	static testMethod void lightTest(){
		
		list<FeedItem> lstFeedItem = new list<FeedItem>();
		
		Light__c l = new Light__c();
		l.Name = 'Test';
		l.PIN__c = 13;
		insert l; 
		
		Test.startTest();
		
		FeedItem f = new FeedItem();
		f.Body = 'ON';
		f.parentId = l.id;
		lstFeedItem.add( f);
		
		FeedItem f1 = new FeedItem();
		f1.Body = 'OFF';
		f1.parentId = l.id;
		lstFeedItem.add( f1);
		
		insert lstFeedItem;
		
		Test.stopTest();
		
	}   
}